function wrapText(text) {
	const wordPerRow = 36;
	
	let wrappedText = '';
	text.split('\n').forEach(splitedText => {
		const row = Math.floor(splitedText.length / wordPerRow) + 1;
		for (let i = 0; i < row; i++) {
			wrappedText += splitedText.substr(i * wordPerRow, wordPerRow);
			wrappedText += '<br>';
		}
	})


	return wrappedText;
}

function requestTask(taskName) {
	document.getElementById('title').innerHTML = taskName;
	let taskContent = document.getElementById('taskContent');
	chrome.storage.sync.get('list', data => {
		const task = data.list.find( (item, index, array) => {
			return Object.keys(item)[0] === taskName;
		});
		taskContent.innerHTML = wrapText(Object.values(task)[0]);
	})
}

chrome.windows.onFocusChanged.addListener( change => {
	window.close();
});

document.addEventListener('DOMContentLoaded', event => {
	chrome.storage.sync.get('focus', data => {
		requestTask(data.focus);
	})
});