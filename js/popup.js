let taskList = '';

function checkDuplicate(list, itemToBeCheck) {
	return -1 === list.findIndex(element => {
		return Object.keys(element)[0] === Object.keys(itemToBeCheck)[0];
	});
}

function getList(callback) {
	chrome.storage.sync.get('list', data => {
		callback(data.list);
	});
}

function removeTask(event) {
	const taskName = event.path[1].children[0].value;
	getList(currentList => {
		const index = currentList.findIndex(element => {
			return Object.keys(element)[0] === taskName;
		});
		if (index!=-1) {
			currentList.splice(index, 1);
			chrome.storage.sync.set({
				list: currentList
			},() => {
				fetchData();
			});
		}
	});
}

function viewTask(event) {
	const taskName = event.path[1].children[0].value;
	chrome.runtime.sendMessage({
		type:'taskClicked',
		param:{
			taskName:taskName
		}
	});
}

function fetchData() {
	taskList.innerHTML = '';
	chrome.storage.sync.get('list', data => {
		for (let i = 0; i < data.list.length; i++) {

			let taskButton = document.createElement('input');

			let taskitem = document.createElement('ol');
			
			let removeButton = document.createElement('input');

			taskButton.addEventListener('click', event => {
				viewTask(event);
			});

			removeButton.addEventListener('click', event => {
				removeTask(event);
			});

			taskButton.type = 'button';
			taskButton.className = 'taskButton';
			taskButton.value = Object.keys(data.list[i]);
			taskButton.id = 'taskButton';

			taskitem.className = 'borderlist';

			removeButton.type = 'button';
			removeButton.className = 'removeButton';
			removeButton.value = '-';

			taskitem.appendChild(taskButton);
			taskitem.appendChild(removeButton);
			taskList.appendChild(taskitem);
		}
	});
	console.log('FetchComplete.');
}

function addTask(taskname, taskContent) {
	let new_task = {};
	new_task[taskname] = taskContent;
	getList(currentList => {
		if (checkDuplicate(currentList, new_task)) {
			currentList.push(new_task);
			chrome.storage.sync.set({
				list: currentList
			},() => {
				fetchData();
			});
		}
	});
}

function addClick(){
	chrome.runtime.sendMessage({
		type:'addClicked'
	});
}

document.addEventListener('DOMContentLoaded', event => {
	taskList = document.getElementById('taskList');

	document.getElementById('addTaskButton').addEventListener('click', element => {
		addClick();
	});

	fetchData();
})

chrome.runtime.onMessage.addListener(request => {
	if (request.type === 'addTask') {
		this.addTask(request.param.taskName, request.param.taskContent);
	};
});
