function wrapper(text){
	return text.replace(/\n/g,'<br>')
}

function construct_listItem(obj) {
	let listItem = document.createElement('li');
	listItem.innerHTML = '<div class="collapsible-header"><i class="material-icons">event_note</i>' + Object.keys(obj) + '</div>';
	listItem.innerHTML += '<div class="collapsible-body"><span>' + wrapper(Object.values(obj)[0]) + '</span></div>';
	return listItem;
}

document.addEventListener('DOMContentLoaded', () => {
	let elems = document.querySelectorAll('.collapsible');
	let options = {};
	let instances = M.Collapsible.init(elems, options);

	let list = document.querySelector('.collapsible')

	chrome.storage.sync.get('list', data => {
		for (let i = 0; i < data.list.length; i++) {
			const item = construct_listItem(data.list[i]);
			list.appendChild(item);
		}
	})

});