document.addEventListener('DOMContentLoaded', event => {
	document.forms[0].onsubmit = event => {
	    event.preventDefault(); 
	    let taskName = document.getElementById('taskName').value;
	    let taskContent = document.getElementById('taskContent').value;
	    chrome.runtime.sendMessage({
	    	type:'addTask',
	    	param:{
	    		taskName:taskName,
	    		taskContent:taskContent
	    	}
	    },() => {
	    	window.close();
	    })
	};
});
