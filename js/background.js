function taskFocus(taskName) {
    chrome.storage.sync.set({
        focus:taskName
    });
}

function taskUnfocus(taskName) {
    chrome.storage.sync.sync({
        focus:''
    });
}

chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.sync.set({list: []}, function() {
        console.log('[info] New Todo-List Created.');
    });
});

chrome.runtime.onMessage.addListener(function(request) {
   switch(request.type){
   case 'addClicked':
        chrome.tabs.create({
            url: chrome.extension.getURL('html/dialog_add.html'),
            active: false
        }, function(tab) {
            chrome.windows.create({
                tabId: tab.id,
                type: 'popup',
                width: 320,
                height: 240,
                focused: true,
            });
        });
        break;
    case 'taskClicked':
        taskFocus(request.param.taskName);
        console.log("view");
        chrome.tabs.create({
            url: chrome.extension.getURL('html/taskContent.html'),
            active: false
        }, function(tab) {
            chrome.windows.create({
                tabId: tab.id,
                type: 'popup',
                width: 333,
                height: 240,
                focused: true,
            });
        });
        break;
    }
});